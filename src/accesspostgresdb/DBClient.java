
package accesspostgresdb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import net.efabrika.util.DBTablePrinter;
import accesspostgresdb.data.Caregiver;
import accesspostgresdb.data.Location;
import accesspostgresdb.data.Patient;

public class DBClient extends AccessData {
  
    public int insertLocation (String name, String region) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;

        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("INSERT INTO LOCATION (NAME,REGION)");
            sb.append("VALUES ( ?, ?) ");
                                   
            String sqlStr = sb.toString();
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1, name);
            pstmt.setString(2, region);
              // execute insert SQL stetement
            pstmt .executeUpdate();
           

        }catch(Exception e){
            System.out.println(e.getMessage());
           
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
      public int insertPatient (String IDCardNum, String name,String surname, String dob, int patientAge,String contact, double temperature
          ,double bodyweight,double height, double blood_Pressure,double pulse ,double sugar,double BMI,String comment,int locationid,int caregiverid ) {
     
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;

        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("INSERT INTO PATIENT  (idcardnum,name,surname,date_of_birth,patientage,contact,");
             sb.append("temperature,bodyweight,height,blood,pulse,sugar,bmi,comment,locationid,caregiverid) ");
            sb.append("VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?) ");
                              
            String sqlStr = sb.toString();
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1, IDCardNum);
            pstmt.setString(2, name);
            pstmt.setString(3, surname);
            pstmt.setString(4, dob);
            pstmt.setInt(5, patientAge);
            pstmt.setString(6, contact);
            pstmt.setDouble(7, temperature);
            pstmt.setDouble(8, bodyweight);
            pstmt.setDouble(9, height);
            pstmt.setDouble(10, blood_Pressure);
            pstmt.setDouble(11,pulse);
            pstmt.setDouble(12, sugar);
            pstmt.setDouble(13, BMI);
            pstmt.setString(14, comment);
            pstmt.setInt(15, locationid);
            pstmt.setInt(16, caregiverid);
            
            
              // execute insert SQL stetement
            pstmt .executeUpdate();
           

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
           
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
    
        public int insertCaregiver (String name, String surname, String Gender, String dob,
                String contact,String emailID,String address, int locationid) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
       
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("INSERT INTO CAREGIVER (NAME,SURNAME,GENDER,DATE_OF_BIRTH,CONTACT,EMAILID,ADDRESS,LOCATIONID )");
            sb.append("VALUES ( ?, ?, ?, ?, ?, ?,?,?) ");
                                   
            String sqlStr = sb.toString();
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1, name);
            pstmt.setString(2, surname);
            pstmt.setString(3, Gender);
            pstmt.setString(4, dob);
            pstmt.setString(5, contact);
            pstmt.setString(6, emailID);
            pstmt.setString(7, address);
            pstmt.setInt(8, locationid);
              // execute insert SQL stetement
            pstmt .executeUpdate();
           

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
           
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }

  // Get  Patients
    public List<Patient> getPatient() {
        List<Patient> list = new ArrayList<Patient>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();                       
                                
            sb.append("SELECT * FROM PATIENT ORDER BY registrationID"); 
         
            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);
//Retrieve data from the result of the read request 
            while( rs.next() )
            {
                Patient db = new Patient();
                int registrationID = rs.getInt("registrationID");
                db.setRegistrationID(registrationID);
                String IDCardNum = rs.getString("IDCardNum");
                db.setIDCardNum(IDCardNum);
                String name = rs.getString("name");
                db.setName(name);
                String surname = rs.getString("surname");
                db.setSurname(surname);
                String date_of_birth = rs.getString("date_of_birth");
                db.setDate_of_birth(date_of_birth);
                int patientAge = rs.getInt("patientAge");
                db.setPatientAge(patientAge);
                String contact = rs.getString("contact");
                db.setContact(contact);              
                float temperature =rs.getFloat("temperature");
                db.setTemperature(temperature);
                float bodyweight =rs.getFloat("bodyweight");
                db.setBodyweight(bodyweight);
                float height = rs.getFloat("height");
                db.setHeight(height);
                float blood = rs.getFloat("blood");
                db.setBlood_Pressure(blood);
                float pulse = rs.getFloat("pulse");
                db.setPulse(pulse);
                float sugar = rs.getFloat("sugar");
                db.setPulse(sugar);
                float bmi = rs.getFloat("bmi");
                db.setBMI(bmi);
                String comment =rs.getString("comment");
                db.setComment(comment);
                int locationId = rs.getInt("locationId");
                db.setLocationId(locationId);
                int caregiverId = rs.getInt("caregiverId");
                db.setCaregiverId(caregiverId);
                list.add(db);
//                           
//                System.out.println(registrationID + "|" +IDCardNum + "|" + name+"|"+surname+"|"+date_of_birth+"|"+patientAge+"|"+contact+"|"
//                        +temperature+"|"+bodyweight+"|"+height+"|"+blood+"|"+pulse+"|"+sugar+"|"+bmi+"|"+comment+"|"+locationId+"|"+caregiverId);
                  DBTablePrinter.printResultSet(rs);
            }

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
           
          
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
  // Get Caregivers
    public List<Caregiver> getCaregiver() {
        List<Caregiver> list = new ArrayList<Caregiver>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();                       
                                
            sb.append("SELECT * FROM CAREGIVER ORDER BY caregiverId"); 
         
            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);
//Retrieve data from the result of the read request
            while( rs.next() )
            {
                Caregiver dbs = new Caregiver();
                int caregiverId = rs.getInt("caregiverId");
                dbs.setCaregiverId(caregiverId);
                String name = rs.getString("name");
                dbs.setName(name);
                String surname = rs.getString("surname");
                dbs.setSurname(surname);
                String date_of_birth = rs.getString("date_of_birth");
                dbs.setDate_of_birth(date_of_birth);
                String contact = rs.getString("contact");
                dbs.setContact(contact);              
                String emailID =rs.getString("emailID");
                dbs.setEmailID(emailID);
                String address = rs.getString("address");
                dbs.setContact(contact);
                list.add(dbs);
                DBTablePrinter.printResultSet(rs);
            }

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
           
          
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
  // Get locations
    public List<Location> getLocation() {
        List<Location> list = new ArrayList<Location>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();                       
                                
            sb.append("SELECT * FROM LOCATION ORDER BY locationId"); 
         
            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);
//Retrieve data from the result of the read request
            while( rs.next() )
            {
                Location dbl = new Location();
                int locationId = rs.getInt("locationId");
                dbl.setLocationId(locationId);
                String name = rs.getString("name");
                dbl.setName(name);
                String region = rs.getString("region");
                dbl.setRegion(region);
                list.add(dbl);
 
                DBTablePrinter.printResultSet(rs);
            }

        }
        catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
           
          
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
  
    //Update Patient
    public int updatePatient ( String valueContact, String cardNum ) {
                
        int resp = 0;
        Connection conn = null;
        Statement stmt = null;
        
        try{
            
            conn = getConnection();
            StringBuilder sb = new StringBuilder();

            
            sb.append("UPDATE PATIENT SET contact = "+valueContact+" WHERE IDCardNum = "+cardNum);
               //UPDATE PATIENT SET contact = '662249089' WHERE IDCardNum = 'CNI06'         
            String sqlStr = sb.toString();

            stmt = conn.createStatement();
            resp = stmt.executeUpdate(sqlStr);


        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{

                try{stmt.close();}catch(Exception e){}
                try{conn.close();}catch(Exception e){}
        }
        return resp;
    }
    
    //Delete a row from Patient
    public int DeletePatient (int id  ) {
                
        int resp = 0;
        Connection conn = null;
        Statement stmt = null;
        
        try{
            
            conn = getConnection();
            StringBuilder sb = new StringBuilder();

            
            sb.append("DELETE FROM  PATIENT  WHERE registrationID = "+id);
                        
            String sqlStr = sb.toString();

            stmt = conn.createStatement();
            resp = stmt.executeUpdate(sqlStr);


        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{

                try{stmt.close();}catch(Exception e){}
                try{conn.close();}catch(Exception e){}
        }
        return resp;
    }

//Delete Location
    public int TruncateLocation( ) {
                
        int resp = 0;
        Connection conn = null;
        Statement stmt = null;
        
        try{
            
            conn = getConnection();
            StringBuilder sb = new StringBuilder();

            
            sb.append("TRUNCATE LOCATION");
                        
            String sqlStr = sb.toString();

            stmt = conn.createStatement();
            resp = stmt.executeUpdate(sqlStr);


        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{

                try{stmt.close();}catch(Exception e){}
                try{conn.close();}catch(Exception e){}
        }
        return resp;
    }


    
    
}

