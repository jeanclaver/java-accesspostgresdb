
package accesspostgresdb;

import mtnz.util.logging.MLogger;

public class AppTester {
    private void InsertEntryTest(){
        DBSrverClient client = new DBSrverClient();
        try{
            
          
        client.insertTransaction("662249089", "105SMS",1100, 105, "7", "1588862458900160000", 1, "SUCCESS", 20);
        client.insertTransaction("664222544", "35SMS",500, 35, "7", "18900160000", 1, "SUCCESS", 20);
        client.insertTransaction("661788909", "80SMS",1200, 80, "7", "158886245800", 1, "SUCCESS", 20);
        }catch (Exception e) {
              System.err.println("Got an exception! "); 
              System.out.println(e.getMessage());
              System.out.println("Insert  Data in  Sms_bundle_transaction Table !");
        }    
    }
    
    private void InsertSharingTest(){
        DBSrverClient client = new DBSrverClient();
        try{
            
          
        client.InsertSharing("662249089","664557677", 51, 35, "15888624589001600078787", "SUCCESS");
        client.InsertSharing("660995690","664222545", 51, 100, "89888624589001600078787", "SUCCESS");
        client.InsertSharing("662249089","664222544", 51, 35, "500078787", "SUCCESS");
        }catch (Exception e) {
              System.err.println("Got an exception! "); 
              System.out.println(e.getMessage());
              System.out.println("Insert  Data in  Sms_sharing_transaction Table !");
        }    
    }
    
    private void getSmsBundles(){
        DBSrverClient client = new DBSrverClient();
        try{
            
          
        client.getSmsBundles();
        }catch (Exception e) {
              System.err.println("Got an exception! "); 
              System.out.println(e.getMessage());
              System.out.println("Retreive Data from COOLSMS_BUNDLES Table !");
        }    
    }
    
    private void getBundleKeyword(String smsBundleKeyword){
        DBSrverClient client = new DBSrverClient();
        try{
          
         smsBundleKeyword= "120SMS";
          
        client.getSMSBundles(smsBundleKeyword);
        }catch (Exception e) {
               System.err.println("Got an exception! "); 
               System.out.println(e.getMessage());
               System.out.println("Retreive Data from COOLSMS_BUNDLES Table !");
        }    
    }
    
    private void insertSmsConfigTest(){
        DBSrverClient client = new DBSrverClient();
        try{
         
          
        client.insertSmsConfig("35SMS", "35SMS", 500, 35, 7, 1);
        client.insertSmsConfig("120SMS", "120SMS", 1500, 120, 15, 1);
        client.insertSmsConfig("500SMS", "500SMS", 6000, 500, 30, 1);
        }catch (Exception e) {
               System.err.println("Got an exception! "); 
               System.out.println(e.getMessage());
               System.out.println("Insert Data into  COOLSMS_BUNDLES Table !");
        }    
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        // TODO code application logic here
        AppTester app = new AppTester();
        
//        app.insertSmsConfigTest();
        System.out.println("Retreive Data from COOLSMS_BUNDLES Table !");
        app.InsertSharingTest();
//        app.getSmsBundles();
        String smsBundleKeyword ="120SMS";
//        app.getBundleKeyword(smsBundleKeyword);

       
    }
    
}

