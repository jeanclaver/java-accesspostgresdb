
package accesspostgresdb;

import accesspostgresdb.model.SMSBundles;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import mtnz.util.logging.MLogger;
import net.efabrika.util.DBTablePrinter;

public class DBSrverClient extends AccessData {
    
     public int insertTransaction (String msisdn, String bundlesName, int price, int smsRefill, String validity, String transactionId, int smsDA, String response, int serviceClass) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();

            sb.append("INSERT INTO  ");
            sb.append("SMS_BUNDLES_TRANSACTION ( ");
            sb.append("CUSTOMER_MSISDN, PACKAGE_NAME, PACKAGE_PRICE, SMS_REFILL, DURATION, TRANSACTION_ID, SMS_DA, RESPONSE, SERVICE_CLASS )");
            sb.append("VALUES ( ?,  ?, ?, ?, ?, ?, ?, ?, ?) ");
                                   
            String sqlStr = sb.toString();
           
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1, msisdn);
            pstmt.setString(2, bundlesName);
            pstmt.setInt(3, price);
            pstmt.setInt(4, smsRefill);
            pstmt.setString(5, validity);
            pstmt.setString(6, transactionId);
            pstmt.setInt(7, smsDA);
            pstmt.setString(8, response);
            pstmt.setInt(9, serviceClass);
            // execute insert SQL stetement
            pstmt .executeUpdate();
           

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }  
     
      public SMSBundles getSMSBundles(String smsBundleKeyword) {
        SMSBundles smsKeyword = null;
       
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder(); 
         
            sb.append("select * "); 
            sb.append("from public.\"COOL_SMS_BUNDLES\" ");
            sb.append("where \"SMS_KEYWORD\" = '"+smsBundleKeyword+"' ");

            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if( rs.next() )
            {
                smsKeyword  = new SMSBundles();

                String bundlesName = rs.getString("BUNDLES_NAME");
                smsKeyword.setBundlesName(bundlesName);
                String sms_Keyword = rs.getString("SMS_KEYWORD");
                smsKeyword.setSmsKeyword(sms_Keyword);
                int price = rs.getInt("PRICE");
                smsKeyword.setPrice(price);
                int smsRefill = rs.getInt("SMS_REFILL");
                smsKeyword.setSmsRefill(smsRefill);
                int validity = rs.getInt("VALIDITY");
                smsKeyword.setValidity(validity);
                int smsDA = rs.getInt("SMS_DA");
                smsKeyword.setSmsDA(smsDA); 
                DBTablePrinter.printResultSet(rs);
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return smsKeyword;
    }
      
    public List<SMSBundles> getSmsBundles() {
        List<SMSBundles> list = new ArrayList<SMSBundles>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();                       
                                
            sb.append("SELECT \"BUNDLES_NAME\", \"SMS_KEYWORD\", \"PRICE\", \"SMS_REFILL\", \"VALIDITY\", \"SMS_DA\"\n" +
                        " FROM public.\"COOL_SMS_BUNDLES\";"); 
         
            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                SMSBundles db = new SMSBundles();
               
                String BUNDLES_NAME = rs.getString("BUNDLES_NAME");
                db.setBundlesName(BUNDLES_NAME);
                String SMS_KEYWORD = rs.getString("SMS_KEYWORD");
                db.setSmsKeyword(SMS_KEYWORD);
                int PRICE = rs.getInt("PRICE");
                db.setPrice(PRICE);            
                int SMS_REFILL = rs.getInt("SMS_REFILL");
                db.setSmsRefill(SMS_REFILL);
                int VALIDITY = rs.getInt("VALIDITY");
                db.setValidity(VALIDITY);
                int SMS_DA = rs.getInt("SMS_DA");
                db.setSmsDA(SMS_DA);
                list.add(db);
//                           
//                System.out.println(registrationID + "|" +IDCardNum + "|" + name+"|"+surname+"|"+date_of_birth+"|"+patientAge+"|"+contact+"|"
//                        +temperature+"|"+bodyweight+"|"+height+"|"+blood+"|"+pulse+"|"+sugar+"|"+bmi+"|"+comment+"|"+locationId+"|"+caregiverId);
                  DBTablePrinter.printResultSet(rs);
            }

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
           
          
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
    
public int InsertSharing (String A_PARTY_MSISDN, String B_PARTY_MSISDN, int B_PARTY_SMS_DA, int SHARED_VOLUME_SMS, String TRANSACTION_ID, String response) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();

            sb.append("INSERT INTO public.\"SMS_SHARING_TRANSACTION\"(\n" +
            "\"A_PARTY_MSISDN\", \"B_PARTY_MSISDN\", \"B_PARTY_SMS_DA\", \"SHARED_VOLUME_SMS\", \"TRANSACTION_ID\", \"RESPONSE\")");
            sb.append("VALUES ( ?, ?,  ?, ?, ?,  ?) ");
                                   
            String sqlStr = sb.toString();
           
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1, A_PARTY_MSISDN);
            pstmt.setString(2, B_PARTY_MSISDN);
            pstmt.setInt(3, B_PARTY_SMS_DA);
            pstmt.setInt(4, SHARED_VOLUME_SMS);
            pstmt.setString(5, TRANSACTION_ID);
            pstmt.setString(6, response);
           
            // execute insert SQL stetement
            pstmt .executeUpdate();
           

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }  

 public int insertSmsConfig (String BUNDLES_NAME, String SMS_KEYWORD, int PRICE, int SMS_REFILL,  int VALIDITY, int SMS_DA) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();

            sb.append("INSERT INTO public.\"COOL_SMS_BUNDLES\"(\n" +
"	\"BUNDLES_NAME\", \"SMS_KEYWORD\", \"PRICE\", \"SMS_REFILL\", \"VALIDITY\", \"SMS_DA\")");
           
            sb.append("VALUES ( ?,  ?, ?, ?, ?, ?) ");
                                   
            String sqlStr = sb.toString();
           
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1, BUNDLES_NAME);
            pstmt.setString(2, SMS_KEYWORD);
            pstmt.setInt(3, PRICE);
            pstmt.setInt(4, SMS_REFILL);
            pstmt.setInt(5, VALIDITY);
            pstmt.setInt(6, SMS_DA);
            // execute insert SQL stetement
            pstmt .executeUpdate();
           

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }  
}
