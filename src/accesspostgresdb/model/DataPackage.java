/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesspostgresdb.model;

import java.io.Serializable;

/**
  
 *
 * @author Administrateur
 */
public class DataPackage implements Serializable {
    
    private String packageType;
    private String addonkeyword;
    private int addonprice;
    private String addonconfirmationTxt;
    private String addonkeywordmomo;
    private String addonheader;
    private String packageName;
    private int price;
    private String keyword;
    private String keywordMoMo;
    private String moMoTxt;
    private String creditTxt;
    private String confirmationTxt;
    private int packageId;
    private String Duration;
    private int packageOrder;

 
    /**
     * @return the packageType
     */
    public String getPackageType() {
        return packageType;
    }

    /**
     * @param packageType the packageType to set
     */
    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    /**
     * @return the packageName
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * @param packageName the packageName to set
     */
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price){
        this.price = price;
    }
 /**
     * @return the addonprice
     */
    public int getAddonPrice(){
        return addonprice;
    }

    /**
     * @param addonprice the addonprice to set
     */
    public void setAddonPrice(int addonprice) {
        this.addonprice = addonprice;
    }

    /**
     * @return the keyword
     */
    public String getKeyword() {
        return keyword;
    }

    public int getPackageOrder() {
        return packageOrder;
    }

    public void setPackageOrder(int packageOrder) {
        this.packageOrder = packageOrder;
    }

    /**
     * @param keyword the keyword to set
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String Duration) {
        this.Duration = Duration;
    }
    
 /**
     * @return the addonkeyword
     */
    public String getAddonKeyword() {
        return addonkeyword;
    }

    /**
     * @param addonkeyword the addonkeyword to set
     */
    public void setAddonKeyword(String addonkeyword) {
        this.addonkeyword = addonkeyword;
    }
    /**
     * @return the keywordMoMo
     */
    public String getKeywordMoMo() {
        return keywordMoMo;
    }

    /**
     * @param keywordMoMo the keywordMoMo to set
     */
    public void setKeywordMoMo(String keywordMoMo) {
        this.keywordMoMo = keywordMoMo;
    }
/**
     * @return the addonkeywordmomo
     */
    public String getAddonKeywordMomo() {
        return addonkeywordmomo;
    }

    /**
     * @param addonkeywordmomo the addonkeywordmomo to set
     */
    public void setAddonKeywordMomo(String addonkeywordmomo) {
        this.addonkeywordmomo = addonkeywordmomo;
    }

    /**
     * @return the moMoTxt
     */
    public String getMoMoTxt() {
        return moMoTxt;
    }

    /**
     * @param moMoTxt the moMoTxt to set
     */
    public void setMoMoTxt(String moMoTxt) {
        this.moMoTxt = moMoTxt;
    }

    /**
     * @return the confirmationTxt
     */
    public String getConfirmationTxt() {
        return confirmationTxt;
    }

    /**
     * @param confirmationTxt the confirmationTxt to set
     */
    public void setConfirmationTxt(String confirmationTxt) {
        this.confirmationTxt = confirmationTxt;
    }
/**
     * @return the addonconfirmationTxt
     */
    public String getAddonconfirmationTxt() {
        return addonconfirmationTxt;
    }

    /**
     * @param addonconfirmationTxt the addonconfirmationTxt to set
     */
    public void setAddonconfirmationTxt(String addonconfirmationTxt) {
        this.addonconfirmationTxt = addonconfirmationTxt;
    }
    /**
     * @return the creditTxt
     */
    public String getCreditTxt() {
        return creditTxt;
    }

    /**
     * @param creditTxt the creditTxt to set
     */
    public void setCreditTxt(String creditTxt) {
        this.creditTxt = creditTxt;
    }
   /**
     * @return the addonheader
     */
    public String getAddonheader() {
        return addonheader;
    }

    /**
     * @param addonheader the addonheader to set
     */
    public void setAddonheader(String addonheader) {
        this.addonheader = addonheader;
    }

    public String getAddonkeyword() {
        return addonkeyword;
    }

    public void setAddonkeyword(String addonkeyword) {
        this.addonkeyword = addonkeyword;
    }

//    public int getAddonprice() {
//        return addonprice;
//    }
//
//    public void setAddonprice(int addonprice) {
//        this.addonprice = addonprice;
//    }

//    public String getAddonkeywordmomo() {
//        return addonkeywordmomo;
//    }
//
//    public void setAddonkeywordmomo(String addonkeywordmomo) {
//        this.addonkeywordmomo = addonkeywordmomo;
//    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }
   

}
