/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesspostgresdb.model;


import java.io.Serializable;

/**
 *
 * @author JCMoutoh
 */
public class SMSBundles implements Serializable {
    //BUNDLES_NAME, SMS_KEYWORD, PRICE, SMS_REFILL, VALIDITY, SMS_DA
    private String bundlesName;
    private String smsKeyword;
    private int price;
    private int smsRefill;
    private int validity;
    private int smsDA;

    /**
     * @return the bundlesName
     */
    public String getBundlesName() {
        return bundlesName;
    }

    /**
     * @param bundlesName the bundlesName to set
     */
    public void setBundlesName(String bundlesName) {
        this.bundlesName = bundlesName;
    }

    /**
     * @return the smsKeyword
     */
    public String getSmsKeyword() {
        return smsKeyword;
    }

    /**
     * @param smsKeyword the smsKeyword to set
     */
    public void setSmsKeyword(String smsKeyword) {
        this.smsKeyword = smsKeyword;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @return the smsRefill
     */
    public int getSmsRefill() {
        return smsRefill;
    }

    /**
     * @param smsRefill the smsRefill to set
     */
    public void setSmsRefill(int smsRefill) {
        this.smsRefill = smsRefill;
    }

    /**
     * @return the validity
     */
    public int getValidity() {
        return validity;
    }

    /**
     * @param validity the validity to set
     */
    public void setValidity(int validity) {
        this.validity = validity;
    }

    /**
     * @return the smsDA
     */
    public int getSmsDA() {
        return smsDA;
    }

    /**
     * @param smsDA the smsDA to set
     */
    public void setSmsDA(int smsDA) {
        this.smsDA = smsDA;
    }

    
}
