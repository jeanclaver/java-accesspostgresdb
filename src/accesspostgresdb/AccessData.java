package accesspostgresdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 *
 * @author JcMoutoh
 */
public  abstract class AccessData {
    
    protected String dbIP = "10.4.0.248" , dbPort = "5432" , dbName = "smsbundle" , dbPass = "app_user" , dbUser = "app_user";
    
    protected Connection getConnection() throws SQLException,ClassNotFoundException,InstantiationException,IllegalAccessException {
       
        Class.forName("org.postgresql.Driver");
        String serverIP = this.dbIP;
        String portNumber = this.dbPort;
        String sid = this.dbName;
        String url = "jdbc:postgresql://"+serverIP+":"+portNumber+"/"+sid+"";
//        String url = "jdbc:postgresql://10.4.0.248:5432/ehealth";
        String username = this.dbUser;
        String password = this.dbPass;
        Connection conn = DriverManager.getConnection(url, username, password);
        System.out.println("Connexion effective !");
        return conn;
   
    }
    
    
}
