/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesspostgresdb.data;
import java.io.Serializable;
import java.util.Date;
/**
 *
 * @author EKhosa
 */
public class DailyBundleMSISDN implements Serializable {
    
    private int msisdn;
    private String segmentId;   

    /**
     * @return the msisdn
     */
    public int getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn the msisdn to set
     */
    public void setMsisdn(int msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * @return the segmentId
     */
    public String getSegmentId() {
        return segmentId;
    }

    /**
     * @param segmentId the segmentId to set
     */
    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }    
}
