/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesspostgresdb.data;

import java.io.Serializable;

/**
 *
 * @author EKhosa
 */
public class DataPackage implements Serializable {
    
    private String packageType;
    private String packageName;
    private int price;
    private String keyword;
    private String keywordMoMo;
    private String moMoTxt;
    private String creditTxt;
    private String confirmationTxt;

    /**
     * @return the packageType
     */
    public String getPackageType() {
        return packageType;
    }

    /**
     * @param packageType the packageType to set
     */
    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    /**
     * @return the packageName
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * @param packageName the packageName to set
     */
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @return the keyword
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * @param keyword the keyword to set
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * @return the keywordMoMo
     */
    public String getKeywordMoMo() {
        return keywordMoMo;
    }

    /**
     * @param keywordMoMo the keywordMoMo to set
     */
    public void setKeywordMoMo(String keywordMoMo) {
        this.keywordMoMo = keywordMoMo;
    }

    /**
     * @return the moMoTxt
     */
    public String getMoMoTxt() {
        return moMoTxt;
    }

    /**
     * @param moMoTxt the moMoTxt to set
     */
    public void setMoMoTxt(String moMoTxt) {
        this.moMoTxt = moMoTxt;
    }

    /**
     * @return the confirmationTxt
     */
    public String getConfirmationTxt() {
        return confirmationTxt;
    }

    /**
     * @param confirmationTxt the confirmationTxt to set
     */
    public void setConfirmationTxt(String confirmationTxt) {
        this.confirmationTxt = confirmationTxt;
    }

    /**
     * @return the creditTxt
     */
    public String getCreditTxt() {
        return creditTxt;
    }

    /**
     * @param creditTxt the creditTxt to set
     */
    public void setCreditTxt(String creditTxt) {
        this.creditTxt = creditTxt;
    }
   

}
