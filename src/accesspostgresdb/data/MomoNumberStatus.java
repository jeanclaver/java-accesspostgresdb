/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesspostgresdb.data;
import java.util.Date;

/**
 *
 * @author JCMoutoh
 */
public class MomoNumberStatus {
    private String msisdn;
    private String keyword;
    private String packageName;
    private Date provisionDate;
    private MomoMsisdn device; 

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Date getProvisionDate() {
        return provisionDate;
    }

    public void setProvisionDate(Date provisionDate) {
        this.provisionDate = provisionDate;
    }

   

    public MomoMsisdn getDevice() {
        return device;
    }

    public void setDevice(MomoMsisdn device) {
        this.device = device;
    }

  
}
