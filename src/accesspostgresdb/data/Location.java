package accesspostgresdb.data;

public class Location {
   	private int locationId;
	private String name;
	private String region;

    public Location(int locationId, String name, String region) {
        this.locationId = locationId;
        this.name = name;
        this.region = region;
    }

    public Location() {
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

   
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
        

}