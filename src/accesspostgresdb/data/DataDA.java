/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesspostgresdb.data;
import accesspostgresdb.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import java.util.*;
/**
 *
 * @author EKhosa
 */
public class DataDA extends AccessData {
    
    
    public DailyBundleMSISDN getMSISDN2(int msisdn) {
        DailyBundleMSISDN resp = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("select * from DailyBundle_MSISDN where msisdn = "+msisdn);
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if( rs.next() )
            {

                String seg = rs.getString("SEGMENT");
                int num = rs.getInt("MSISDN");

                resp = new DailyBundleMSISDN();
                resp.setMsisdn(num);
                resp.setSegmentId(seg);
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
    
    
    
    
    public DataResellerBrandSupport getBrandedSupport(int msisdn) {
        DataResellerBrandSupport resp = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("select * from DataReseller_BrandSupport where msisdn = "+msisdn);
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if( rs.next() )
            {

                int num = rs.getInt("MSISDN");

                resp = new DataResellerBrandSupport();
                resp.setMsisdn(num);           
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
    
    public List<DataResellerCommissionReport> getCommissionsReport(int customerMsisdn, int n) {
        List<DataResellerCommissionReport> list = new ArrayList<DataResellerCommissionReport>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
                        
            sb.append("select * from ( ");
            sb.append("    select created_date, commission from ( ");
            sb.append("        select sum(commission) as commission, created_date from ( ");
            sb.append("            select created_date,commission  from ( ");
            sb.append("                select reseller_msisdn,trunc(created_date) as created_date,commission from DataReseller_commission where reseller_msisdn = "+customerMsisdn+"  ");
            sb.append("            ) ");
            sb.append("        ) group by created_date ");
            sb.append("    ) order by created_date desc ");
            sb.append(") ");
            if(n > 0){
                sb.append("WHERE rownum <= "+n);
            }        
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                DataResellerCommissionReport dc = new DataResellerCommissionReport();
                java.util.Date createdDate = rs.getDate("created_date");
                dc.setCreatedDate(createdDate); 
                float commission = rs.getFloat("commission");
                dc.setCommission(commission);
                list.add(dc);
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
    
    
    
    public int insertTransaction (int msisdn, String transactionId, String packageType,String packageName,int price,String keyword, String response, String mdpResponse) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            
            sb.append("INSERT INTO  ");
            sb.append("DATARESELLER_TRANSACTION ( ");
            sb.append("TRANSACTION_ID,MSISDN, PACKAGE_TYPE,PACKAGE_NAME, PRICE, keyword, response, mdp_response) ");
            sb.append("VALUES (    ?,       ?,      ?,         ?,          ?,    ?,  ?,        ?) ");
                                   
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1,transactionId);
            pstmt.setInt(2, msisdn);
           
            pstmt.setString(3, packageType);
            pstmt.setString(4, packageName);
            pstmt.setInt (5, price);     
           
            pstmt.setString(6, keyword);
            
            pstmt.setString(7,response);
            
            pstmt.setString(8,mdpResponse);
            
            // execute insert SQL stetement
            pstmt .executeUpdate();
            
                  

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }  
    
    
    public int insertCommission (String ressellerMsisdn,String customerMsisdn, String transactionId, int price, float commission, String resellerType ) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            
            sb.append("INSERT INTO APP_USER.DATARESELLER_COMMISSION (   ");
            sb.append("TRANSACTION_ID, ");
            sb.append("RESELLER_MSISDN, ");
            sb.append("CUSTOMER_MSISDN, ");
            sb.append("PRICE, ");
            sb.append("COMMISSION, ");
            sb.append("RESELLER_TYPE ) ");
            sb.append("VALUES ( ");
            sb.append("?, ?, ?, ?, ?, ? ");
            sb.append(")  ");
            
            
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1, transactionId);
            pstmt.setString(2, ressellerMsisdn);
            pstmt.setString(3, customerMsisdn);
            pstmt.setInt(4, price);
            pstmt.setFloat (5, commission);
            pstmt.setString(6, resellerType);
                       
                        
            // execute insert SQL stetement
            pstmt .executeUpdate();
            
                  

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    } 
    
    
   public List<String> getPackageTypes() {
        List<String> list = new ArrayList<String>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
                        
          
            sb.append("select packagetype_name from databundle_packagetype ");
            sb.append("where packagetype_active = 1 ");
            sb.append("order by packagetype_sequence");       
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                String packageType = rs.getString("packagetype_name");
                
                list.add(packageType);
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
   
    public List<DataPackage> getPackages(String packageType) {
        List<DataPackage> list = new ArrayList<DataPackage>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();                       
                                
            sb.append("select * from databundle_package_v2 pkg, databundle_packagetype pkgt "); 
            sb.append("where pkg.PACKAGE_TYPE = pkgt.packagetype_id ");
            sb.append("and pkgt.packagetype_name ='"+packageType+"' and pkg.package_active = 1 ");
            sb.append("order by pkg.package_order ");

            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                DataPackage dc = new DataPackage();
                dc.setPackageType(packageType);
                String packageName = rs.getString("package_name");
                dc.setPackageName(packageName);
                String keyword = rs.getString("package_keyword");
                dc.setKeyword(keyword);
                String keywordMoMo = rs.getString("package_keyword_MoMo");
                dc.setKeywordMoMo(keywordMoMo);
                int price = rs.getInt("package_price");
                dc.setPrice(price);    
                
                String moMoTxt = rs.getString("package_MoMo_Text");
                dc.setMoMoTxt(moMoTxt);
                
                String creditTxt = rs.getString("package_Credit_Text");
                dc.setCreditTxt(creditTxt);
                
                String confirmationTxt = rs.getString("package_confirmation_Text");
                dc.setConfirmationTxt(confirmationTxt);
                
                list.add(dc);
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
    
    public DataPackage getPackagezzzz(String packageName) {
        DataPackage ret = null;
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();                       
          
            sb.append(" select * from databundle_package where package_name='"+packageName+"' and active = 1 ");
            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if( rs.next() )
            {
                ret = new DataPackage();
                String packageType = rs.getString("package_type");
                ret.setPackageType(packageType);
                ret.setPackageName(packageName);
                ret.setPackageName(packageName);
                String keyword = rs.getString("keyword");
                ret.setKeyword(keyword);
                String keywordMoMo = rs.getString("keywordMoMo");
                ret.setKeywordMoMo(keywordMoMo);
                int price = rs.getInt("price");
                ret.setPrice(price);
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return ret;
    }
   
    
}
