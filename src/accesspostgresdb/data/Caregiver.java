package accesspostgresdb.data;

import java.util.Date;

public class Caregiver {
	private int caregiverId;
	private String name;
        private String surname;
        private String date_of_birth;
        private String contact;
        private String emailID;
        private String address;

    public Caregiver(int caregiverId, String name, String surname, String date_of_birth, String contact, String emailID, String address) {
        this.caregiverId = caregiverId;
        this.name = name;
        this.surname = surname;
        this.date_of_birth = date_of_birth;
        this.contact = contact;
        this.emailID = emailID;
        this.address = address;
    }

    public Caregiver() {
    }

    /**
     * @return the caregiverId
     */
    public int getCaregiverId() {
        return caregiverId;
    }

    /**
     * @param caregiverId the caregiverId to set
     */
    public void setCaregiverId(int caregiverId) {
        this.caregiverId = caregiverId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname the surname to set
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return the date_of_birth
     */
    public String getDate_of_birth() {
        return date_of_birth;
    }

    /**
     * @param date_of_birth the date_of_birth to set
     */
    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    /**
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * @return the emailID
     */
    public String getEmailID() {
        return emailID;
    }

    /**
     * @param emailID the emailID to set
     */
    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

   
}