/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesspostgresdb.data;

import java.io.Serializable;
import java.sql.Timestamp;

/*
 *
 * @author Jc Moutoh
 */
public class Package implements Serializable {
    
    private String packageType;
    private String addonkeyword;
    private double addonprice;
    private String addonconfirmationTxt;
    private String addonkeywordmomo;
    private String addonheader;
    private String packageName;
    private double price;
    private String keyword;
    private String keywordMoMo;
    private String moMoTxt;
    private String creditTxt;
    private String confirmationTxt;
    private double packageId;
    private String Duration;
    private double packageOrder;
    private String result;
    private String renewal_response;
    private String renewlal_status;
    private java.sql.Timestamp expiry_date;
    private java.sql.Timestamp renewal_date;
    private String transaction_id;
    private String customer_msisdn;
    private java.sql.Timestamp transaction_date;
   
    
    

    /**
     * @return the packageType
     */
    public String getPackageType() {
        return packageType;
    }

    /**
     * @param packageType the packageType to set
     */
    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    /**
     * @return the addonkeyword
     */
    public String getAddonkeyword() {
        return addonkeyword;
    }

    /**
     * @param addonkeyword the addonkeyword to set
     */
    public void setAddonkeyword(String addonkeyword) {
        this.addonkeyword = addonkeyword;
    }

    /**
     * @return the addonprice
     */
    public double getAddonprice() {
        return addonprice;
    }

    /**
     * @param addonprice the addonprice to set
     */
    public void setAddonprice(double addonprice) {
        this.addonprice = addonprice;
    }

    /**
     * @return the addonconfirmationTxt
     */
    public String getAddonconfirmationTxt() {
        return addonconfirmationTxt;
    }

    /**
     * @param addonconfirmationTxt the addonconfirmationTxt to set
     */
    public void setAddonconfirmationTxt(String addonconfirmationTxt) {
        this.addonconfirmationTxt = addonconfirmationTxt;
    }

    /**
     * @return the addonkeywordmomo
     */
    public String getAddonkeywordmomo() {
        return addonkeywordmomo;
    }

    /**
     * @param addonkeywordmomo the addonkeywordmomo to set
     */
    public void setAddonkeywordmomo(String addonkeywordmomo) {
        this.addonkeywordmomo = addonkeywordmomo;
    }

    /**
     * @return the addonheader
     */
    public String getAddonheader() {
        return addonheader;
    }

    /**
     * @param addonheader the addonheader to set
     */
    public void setAddonheader(String addonheader) {
        this.addonheader = addonheader;
    }

    /**
     * @return the packageName
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * @param packageName the packageName to set
     */
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the keyword
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * @param keyword the keyword to set
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * @return the keywordMoMo
     */
    public String getKeywordMoMo() {
        return keywordMoMo;
    }

    /**
     * @param keywordMoMo the keywordMoMo to set
     */
    public void setKeywordMoMo(String keywordMoMo) {
        this.keywordMoMo = keywordMoMo;
    }

    /**
     * @return the moMoTxt
     */
    public String getMoMoTxt() {
        return moMoTxt;
    }

    /**
     * @param moMoTxt the moMoTxt to set
     */
    public void setMoMoTxt(String moMoTxt) {
        this.moMoTxt = moMoTxt;
    }

    /**
     * @return the creditTxt
     */
    public String getCreditTxt() {
        return creditTxt;
    }

    /**
     * @param creditTxt the creditTxt to set
     */
    public void setCreditTxt(String creditTxt) {
        this.creditTxt = creditTxt;
    }

    /**
     * @return the confirmationTxt
     */
    public String getConfirmationTxt() {
        return confirmationTxt;
    }

    /**
     * @param confirmationTxt the confirmationTxt to set
     */
    public void setConfirmationTxt(String confirmationTxt) {
        this.confirmationTxt = confirmationTxt;
    }

    /**
     * @return the packageId
     */
    public double getPackageId() {
        return packageId;
    }

    /**
     * @param packageId the packageId to set
     */
    public void setPackageId(double packageId) {
        this.packageId = packageId;
    }

    /**
     * @return the Duration
     */
    public String getDuration() {
        return Duration;
    }

    /**
     * @param Duration the Duration to set
     */
    public void setDuration(String Duration) {
        this.Duration = Duration;
    }

    /**
     * @return the packageOrder
     */
    public double getPackageOrder() {
        return packageOrder;
    }

    /**
     * @param packageOrder the packageOrder to set
     */
    public void setPackageOrder(double packageOrder) {
        this.packageOrder = packageOrder;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getRenewal_response() {
        return renewal_response;
    }

    public void setRenewal_response(String renewal_response) {
        this.renewal_response = renewal_response;
    }

    public String getRenewlal_status() {
        return renewlal_status;
    }

    public void setRenewlal_status(String renewlal_status) {
        this.renewlal_status = renewlal_status;
    }

    public Timestamp getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(Timestamp expiry_date) {
        this.expiry_date = expiry_date;
    }

    public Timestamp getRenewal_date() {
        return renewal_date;
    }

    public void setRenewal_date(Timestamp renewal_date) {
        this.renewal_date = renewal_date;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getCustomer_msisdn() {
        return customer_msisdn;
    }

    public void setCustomer_msisdn(String customer_msisdn) {
        this.customer_msisdn = customer_msisdn;
    }

    public Timestamp getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(Timestamp transaction_date) {
        this.transaction_date = transaction_date;
    }

   
    public Package() {
    }

   
   
    
}
