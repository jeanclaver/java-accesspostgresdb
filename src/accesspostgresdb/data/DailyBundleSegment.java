/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesspostgresdb.data;
import java.io.Serializable;

/**
 *
 * @author EKhosa
 */
public class DailyBundleSegment implements Serializable {
    
    private String segmentId;
    private int priceJunior;
    private int priceMajor;
    private int priceSenior;
    private int refillONNETJunior;
    private int refillONNETMajor;
    private int refillONNETSenior;
    private int refillAllDestJunior;
    private int refillAllDestMajor;
    private int refillAllDestSenior;
    private int refillSMS;
    private int refillDATA;
    private String refillDATAKeyword;

    /**
     * @return the segmentId
     */
    public String getSegmentId() {
        return segmentId;
    }

    /**
     * @param segmentId the segmentId to set
     */
    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

    /**
     * @return the priceJunior
     */
    public int getPriceJunior() {
        return priceJunior;
    }

    /**
     * @param priceJunior the priceJunior to set
     */
    public void setPriceJunior(int priceJunior) {
        this.priceJunior = priceJunior;
    }

    /**
     * @return the priceMajor
     */
    public int getPriceMajor() {
        return priceMajor;
    }

    /**
     * @param priceMajor the priceMajor to set
     */
    public void setPriceMajor(int priceMajor) {
        this.priceMajor = priceMajor;
    }

    /**
     * @return the priceSenior
     */
    public int getPriceSenior() {
        return priceSenior;
    }

    /**
     * @param priceSenior the priceSenior to set
     */
    public void setPriceSenior(int priceSenior) {
        this.priceSenior = priceSenior;
    }

    /**
     * @return the refillONNETJunior
     */
    public int getRefillONNETJunior() {
        return refillONNETJunior;
    }

    /**
     * @param refillONNETJunior the refillONNETJunior to set
     */
    public void setRefillONNETJunior(int refillONNETJunior) {
        this.refillONNETJunior = refillONNETJunior;
    }

    /**
     * @return the refillONNETMajor
     */
    public int getRefillONNETMajor() {
        return refillONNETMajor;
    }

    /**
     * @param refillONNETMajor the refillONNETMajor to set
     */
    public void setRefillONNETMajor(int refillONNETMajor) {
        this.refillONNETMajor = refillONNETMajor;
    }

    /**
     * @return the refillONNETSenior
     */
    public int getRefillONNETSenior() {
        return refillONNETSenior;
    }

    /**
     * @param refillONNETSenior the refillONNETSenior to set
     */
    public void setRefillONNETSenior(int refillONNETSenior) {
        this.refillONNETSenior = refillONNETSenior;
    }

    /**
     * @return the refillAllDestJunior
     */
    public int getRefillAllDestJunior() {
        return refillAllDestJunior;
    }

    /**
     * @param refillAllDestJunior the refillAllDestJunior to set
     */
    public void setRefillAllDestJunior(int refillAllDestJunior) {
        this.refillAllDestJunior = refillAllDestJunior;
    }

    /**
     * @return the refillAllDestMajor
     */
    public int getRefillAllDestMajor() {
        return refillAllDestMajor;
    }

    /**
     * @param refillAllDestMajor the refillAllDestMajor to set
     */
    public void setRefillAllDestMajor(int refillAllDestMajor) {
        this.refillAllDestMajor = refillAllDestMajor;
    }

    /**
     * @return the refillAllDestSenior
     */
    public int getRefillAllDestSenior() {
        return refillAllDestSenior;
    }

    /**
     * @param refillAllDestSenior the refillAllDestSenior to set
     */
    public void setRefillAllDestSenior(int refillAllDestSenior) {
        this.refillAllDestSenior = refillAllDestSenior;
    }

    /**
     * @return the refillSMS
     */
    public int getRefillSMS() {
        return refillSMS;
    }

    /**
     * @param refillSMS the refillSMS to set
     */
    public void setRefillSMS(int refillSMS) {
        this.refillSMS = refillSMS;
    }

    /**
     * @return the refillDATA
     */
    public int getRefillDATA() {
        return refillDATA;
    }

    /**
     * @param refillDATA the refillDATA to set
     */
    public void setRefillDATA(int refillDATA) {
        this.refillDATA = refillDATA;
    }

    /**
     * @return the refillDATAKeyword
     */
    public String getRefillDATAKeyword() {
        return refillDATAKeyword;
    }

    /**
     * @param refillDATAKeyword the refillDATAKeyword to set
     */
    public void setRefillDATAKeyword(String refillDATAKeyword) {
        this.refillDATAKeyword = refillDATAKeyword;
    }
    

}
