/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesspostgresdb.data;

import java.util.Date;

/**
 *
 * @author ekhosa
 */
public class DailyBundleSponsor {
    private int resellerMsisdn;
    private int customerMsisdn;
    private Date startDate;
    private Date expiryDate;
    private String sponsorType;

    /**
     * @return the resellerMsisdn
     */
    public int getResellerMsisdn() {
        return resellerMsisdn;
    }

    /**
     * @param resellerMsisdn the resellerMsisdn to set
     */
    public void setResellerMsisdn(int resellerMsisdn) {
        this.resellerMsisdn = resellerMsisdn;
    }

    /**
     * @return the customerMsisdn
     */
    public int getCustomerMsisdn() {
        return customerMsisdn;
    }

    /**
     * @param customerMsisdn the customerMsisdn to set
     */
    public void setCustomerMsisdn(int customerMsisdn) {
        this.customerMsisdn = customerMsisdn;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the expiryDate
     */
    public Date getExpiryDate() {
        return expiryDate;
    }

    /**
     * @param expiryDate the expiryDate to set
     */
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    /**
     * @return the sponsorType
     */
    public String getSponsorType() {
        return sponsorType;
    }

    /**
     * @param sponsorType the sponsorType to set
     */
    public void setSponsorType(String sponsorType) {
        this.sponsorType = sponsorType;
    }
    
}
