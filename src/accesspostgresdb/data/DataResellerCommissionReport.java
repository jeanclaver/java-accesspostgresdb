/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesspostgresdb.data;



/**
 *
 * @author ekhosa
 */
public class DataResellerCommissionReport {
    private java.util.Date createdDate;
    private float commission;

    /**
     * @return the createdDate
     */
    public java.util.Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(java.util.Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the commission
     */
    public float getCommission() {
        return commission;
    }

    /**
     * @param commission the commission to set
     */
    public void setCommission(float commission) {
        this.commission = commission;
    }
    

}
