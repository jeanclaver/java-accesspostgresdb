/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesspostgresdb.data;
import accesspostgresdb.AccessData;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author JcMoutoh
 */
public class DavidoDBClient extends AccessData {
    
 

    public int insertEntry (int msisdn, String transactionId,String entryType, int price,double balanceBefore,double balanceAfter,String cellId,String region ) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            
            sb.append("INSERT INTO  ");
            sb.append("TBL_DAVIDO_ENTRY ( ");
            sb.append("DATE_CREATED, MSISDN, TRANSACTION_ID, ENTRY_TYPE, BALANCE_BEFORE, PRICE, BALANCE_AFTER, CELLID, REGION) ");
            sb.append("VALUES (sysdate,     ?,       ?,              ?,          ?,     ?,                ?,?,?) ");
            
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, msisdn);
            pstmt.setString(2, transactionId);
            pstmt.setString(3, entryType);
            pstmt.setDouble(4, balanceBefore);
            pstmt.setInt (5, price);
            pstmt.setDouble(6, balanceAfter);
            
            if(cellId != null){
                pstmt.setString(7, cellId);
            }else{
                pstmt.setNull(7, java.sql.Types.VARCHAR);
            }
            
            if(region != null){
                pstmt.setString(8, region);
            }else{
                pstmt.setNull(8, java.sql.Types.VARCHAR);
            }
            
                        
            // execute insert SQL stetement
            pstmt .executeUpdate();
            
                  

        }catch(Exception e){
           System.err.println("Got an exception! "); 
           System.out.println(e.getMessage());
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }  
        
    
}
