package accesspostgresdb.data;
import java.util.Date;

public class Patient {

	private int registrationID;
	private String IDCardNum;
	private String name;
        private String surname;
        private String date_of_birth;
        private String contact;
        private int patientAge;
	private double temperature;
	private double bodyweight;
        private double height;
	private double blood_Pressure;
	private double pulse;
	private double sugar;
        private double BMI;
	private String comment;
        private int caregiverId;
        private int locationId;
    
        
        public Patient(int registrationID, String IDCardNum, String name, String surname, String date_of_birth, String contact, int patientAge, double temperature, double bodyweight, double height, double blood_Pressure, double pulse, double sugar, double BMI, String comment, int caregiverId, int locationId) {
        this.registrationID = registrationID;
        this.IDCardNum = IDCardNum;
        this.name = name;
        this.surname = surname;
        this.date_of_birth = date_of_birth;
        this.contact = contact;
        this.patientAge = patientAge;
        this.temperature = temperature;
        this.bodyweight = bodyweight;
        this.height = height;
        this.blood_Pressure = blood_Pressure;
        this.pulse = pulse;
        this.sugar = sugar;
        this.BMI = BMI;
        this.comment = comment;
        this.caregiverId = caregiverId;
        this.locationId = locationId;
    }


    public Patient() {
    }

    /**
     * @return the registrationID
     */
    public int getRegistrationID() {
        return registrationID;
    }

    /**
     * @param registrationID the registrationID to set
     */
    public void setRegistrationID(int registrationID) {
        this.registrationID = registrationID;
    }

    /**
     * @return the IDCardNum
     */
    public String getIDCardNum() {
        return IDCardNum;
    }

    /**
     * @param IDCardNum the IDCardNum to set
     */
    public void setIDCardNum(String IDCardNum) {
        this.IDCardNum = IDCardNum;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname the surname to set
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return the date_of_birth
     */
    public String getDate_of_birth() {
        return date_of_birth;
    }

    /**
     * @param date_of_birth the date_of_birth to set
     */
    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    /**
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * @return the patientAge
     */
    public int getPatientAge() {
        return patientAge;
    }

    /**
     * @param patientAge the patientAge to set
     */
    public void setPatientAge(int patientAge) {
        this.patientAge = patientAge;
    }

    /**
     * @return the temperature
     */
    public double getTemperature() {
        return temperature;
    }

    /**
     * @param temperature the temperature to set
     */
    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    /**
     * @return the bodyweight
     */
    public double getBodyweight() {
        return bodyweight;
    }

    /**
     * @param bodyweight the bodyweight to set
     */
    public void setBodyweight(double bodyweight) {
        this.bodyweight = bodyweight;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * @return the blood_Pressure
     */
    public double getBlood_Pressure() {
        return blood_Pressure;
    }

    /**
     * @param blood_Pressure the blood_Pressure to set
     */
    public void setBlood_Pressure(double blood_Pressure) {
        this.blood_Pressure = blood_Pressure;
    }

    /**
     * @return the pulse
     */
    public double getPulse() {
        return pulse;
    }

    /**
     * @param pulse the pulse to set
     */
    public void setPulse(double pulse) {
        this.pulse = pulse;
    }

    /**
     * @return the sugar
     */
    public double getSugar() {
        return sugar;
    }

    /**
     * @param sugar the sugar to set
     */
    public void setSugar(double sugar) {
        this.sugar = sugar;
    }

    /**
     * @return the BMI
     */
    public double getBMI() {
        return BMI;
    }

    /**
     * @param BMI the BMI to set
     */
    public void setBMI(double BMI) {
        this.BMI = BMI;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getCaregiverId() {
        return caregiverId;
    }

    public void setCaregiverId(int caregiverId) {
        this.caregiverId = caregiverId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    
   
   
}