/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesspostgresdb.data;
import java.util.*;
/**
 *
 * @author ekhosa
 */
public class DailyBundleTransaction {
    private Date createdDate;
    private java.sql.Timestamp expiryDate;
    private int MSISDN;
    private String segment;
    private int onnetDA,alldestDA, smsDA;
    private String transactionId;
    private int onnetRefill,alldestRefill, smsRefill;
    

    /**
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the MSISDN
     */
    public int getMSISDN() {
        return MSISDN;
    }

    /**
     * @param MSISDN the MSISDN to set
     */
    public void setMSISDN(int MSISDN) {
        this.MSISDN = MSISDN;
    }

    /**
     * @return the segment
     */
    public String getSegment() {
        return segment;
    }

    /**
     * @param segment the segment to set
     */
    public void setSegment(String segment) {
        this.segment = segment;
    }

    /**
     * @return the onnetDA
     */
    public int getOnnetDA() {
        return onnetDA;
    }

    /**
     * @param onnetDA the onnetDA to set
     */
    public void setOnnetDA(int onnetDA) {
        this.onnetDA = onnetDA;
    }

    /**
     * @return the alldestDA
     */
    public int getAlldestDA() {
        return alldestDA;
    }

    /**
     * @param alldestDA the alldestDA to set
     */
    public void setAlldestDA(int alldestDA) {
        this.alldestDA = alldestDA;
    }

    /**
     * @return the smsDA
     */
    public int getSmsDA() {
        return smsDA;
    }

    /**
     * @param smsDA the smsDA to set
     */
    public void setSmsDA(int smsDA) {
        this.smsDA = smsDA;
    }

    /**
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * @return the expiryDate
     */
    public java.sql.Timestamp getExpiryDate() {
        return expiryDate;
    }

    /**
     * @param expiryDate the expiryDate to set
     */
    public void setExpiryDate(java.sql.Timestamp expiryDate) {
        this.expiryDate = expiryDate;
    }

    /**
     * @return the onnetRefill
     */
    public int getOnnetRefill() {
        return onnetRefill;
    }

    /**
     * @param onnetRefill the onnetRefill to set
     */
    public void setOnnetRefill(int onnetRefill) {
        this.onnetRefill = onnetRefill;
    }

    /**
     * @return the alldestRefill
     */
    public int getAlldestRefill() {
        return alldestRefill;
    }

    /**
     * @param alldestRefill the alldestRefill to set
     */
    public void setAlldestRefill(int alldestRefill) {
        this.alldestRefill = alldestRefill;
    }

    /**
     * @return the smsRefill
     */
    public int getSmsRefill() {
        return smsRefill;
    }

    /**
     * @param smsRefill the smsRefill to set
     */
    public void setSmsRefill(int smsRefill) {
        this.smsRefill = smsRefill;
    }


    
}
