/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesspostgresdb.data;
import accesspostgresdb.AccessData;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import net.efabrika.util.DBTablePrinter;
/**
 *
 * @author JcMoutoh
 */
public class DataClient extends AccessData {
    
    
    public int insertTransactionDetails (String transactionId, String msisdn, String packageType,String packageName,String duration,String expiry_date,double price,String refillid, String result, String renewal_response, String renewlal_status,String renewal_date) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            
            sb.append("INSERT INTO  ");
            sb.append("DATABUNDLE_REVAMP_TRANSACT ( ");
            sb.append("transaction_id,customer_msisdn , package_type,package_name, duration,price, refillid, result, renewal_response,renewlal_status) ");
            sb.append("VALUES (    ?,     ?,    ?,      ?,    ?,   ?,       ?,    ?,  ?,        ?) ");
                                   
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1,transactionId);
            pstmt.setString(2, msisdn);
            pstmt.setString(3, packageType);
            pstmt.setString(4, packageName);
            pstmt.setString(5, duration);
            pstmt.setDouble (6, price);     
            pstmt.setString(7, refillid);
            pstmt.setString(8,result);
            pstmt.setString(9,renewal_response);
            pstmt.setString(10,renewlal_status);
           
            
            // execute insert SQL stetement
            pstmt .executeUpdate();
            
        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    } 
       
     // Get All PackageTypes 
   public List<String> getPackageTypes() {
        List<String> list = new ArrayList<String>();
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("select packagetype_name from mdpswapdatabundle_packagetype_dev ");
            sb.append("where packagetype_active = 1 ");
            sb.append("order by packagetype_sequence");            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                String packageType = rs.getString("packagetype_name");
                
                list.add(packageType);
                 DBTablePrinter.printResultSet(rs);
            }

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
   
    // Get Default  PackageTypes
   public List<String> getDefaultPackageTypes() {
        List<String> list = new ArrayList<String>();
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
                        
            sb.append("select packagetype_name from mdpswapdatabundle_packagetype_dev ");
            sb.append("where packagetype_active = 1 ");
           // sb.append("and  packagetype_id  != 1");
            sb.append("order by packagetype_sequence");        
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                String packageType = rs.getString("packagetype_name");
                
                list.add(packageType);
                 DBTablePrinter.printResultSet(rs);
            }

        }catch(Exception e){
             System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
   // Get all  Bundles
    public List<Package> getPackages(String packageType) {
        List<Package> list = new ArrayList<Package>();
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();                       
                                
            sb.append("select * from mdpswapp_databundle_dev pkg, mdpswapdatabundle_packagetype_dev pkgt "); 
            sb.append("where pkg.PACKAGE_TYPE = pkgt.packagetype_id ");
            sb.append("and pkgt.packagetype_name ='"+packageType+"' and pkg.package_active = 1 ");
            sb.append("order by pkg.package_order ");

            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                Package dc = new Package();
                dc.setPackageType(packageType);
                String packageName = rs.getString("package_name");
                dc.setPackageName(packageName);
                String keyword = rs.getString("package_keyword");
                dc.setKeyword(keyword);
                String addonkeyword = rs.getString("addon_keyword");
                dc.setAddonkeyword(addonkeyword);
                String keywordMoMo = rs.getString("package_keyword_MoMo");
                dc.setKeywordMoMo(keywordMoMo);
                 String addonkeywordmomo = rs.getString("addon_keyword_momo");
                dc.setAddonkeywordmomo(addonkeywordmomo);
                double price = rs.getDouble("package_price");
                dc.setPrice(price); 
                double addonprice = rs.getDouble("addon_package_price");
                dc.setAddonprice(addonprice);
                
                String moMoTxt = rs.getString("package_MoMo_Text");
                dc.setMoMoTxt(moMoTxt);
                
                String creditTxt = rs.getString("package_Credit_Text");
                dc.setCreditTxt(creditTxt);
                
                 String addonheader = rs.getString("addon_header_Text");
                dc.setAddonheader(addonheader);
                
                String confirmationTxt = rs.getString("package_confirmation_Text");
                dc.setConfirmationTxt(confirmationTxt);
                
                String addonconfirmationTxt = rs.getString("addon_package_confirmation_Txt");
                dc.setAddonconfirmationTxt(addonconfirmationTxt);
                String Duration = rs.getString("DURATION");
                dc.setDuration(Duration);
                list.add(dc);
                DBTablePrinter.printResultSet(rs);
            }

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
    
    // Get Default Bundles
    public List<Package> getDefaultPackages(String packageType) {
        List<Package> list = new ArrayList<Package>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();                       
                                
           sb.append("select * from mdpswapp_databundle_dev pkg, mdpswapdatabundle_packagetype_dev pkgt "); 
            sb.append("where pkg.PACKAGE_TYPE = pkgt.packagetype_id ");
           // sb.append("and  packagetype_id  != 1");
            sb.append("and pkgt.packagetype_name ='"+packageType+"' and pkg.package_active = 1 ");
            sb.append("order by pkg.package_order ");

            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                Package db = new Package();
                db.setPackageType(packageType);
                String packageName = rs.getString("package_name");
                db.setPackageName(packageName);
                String keyword = rs.getString("package_keyword");
                db.setKeyword(keyword);
                String addonkeyword = rs.getString("addon_keyword");
                db.setAddonkeyword(addonkeyword);
                String keywordMoMo = rs.getString("package_keyword_MoMo");
                db.setKeywordMoMo(keywordMoMo);
                 String addonkeywordmomo = rs.getString("addon_keyword_momo");
                db.setAddonkeywordmomo(addonkeywordmomo);
                double price = rs.getDouble("package_price");
                db.setPrice(price); 
                double addonprice = rs.getDouble("addon_package_price");
                db.setAddonprice(addonprice);
                
                String moMoTxt = rs.getString("package_MoMo_Text");
                db.setMoMoTxt(moMoTxt);
                
                String creditTxt = rs.getString("package_Credit_Text");
                db.setCreditTxt(creditTxt);
                
                 String addonheader = rs.getString("addon_header_Text");
                db.setAddonheader(addonheader);
                
                String confirmationTxt = rs.getString("package_confirmation_Text");
                db.setConfirmationTxt(confirmationTxt);
                
                String addonconfirmationTxt = rs.getString("addon_package_confirmation_Txt");
                db.setAddonconfirmationTxt(addonconfirmationTxt);
                 String Duration = rs.getString("DURATION");
                db.setDuration(Duration);
              
                list.add(db);
                DBTablePrinter.printResultSet(rs);
            }

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
    
    /*
     Find addon by type ,; name ; price 
    */
    
    public Package getAddonPackages(String packageType , String packageName  , double packageOrder) {
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        Package dataPackage = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();                       
                                
          sb.append("select * from DATABUNDLE_PACKAGE_LIVE pkg, DATABUNDLE_PACKAGETYPE_V4 pkgt "); 
          sb.append(" where pkg.PACKAGE_TYPE = pkgt.packagetype_id ");
          sb.append(" and pkgt.packagetype_name ='"+packageType+"' and pkg.package_active = 1  and PACKAGE_NAME='"+packageName+"'" );
          sb.append(" and pkg.addon_header_text is not null");
          sb.append(" and pkg.package_order = '"+packageOrder+"'");
//          sb.append(" Select * from DATABUNDLE_PACKAGE_V5 where PACKAGE_TYPE= ="+packageType+" and PACKAGE_ORDER="+packageOrder); 
          
          
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                Package dc = new Package();
                dc.setPackageType(packageType);
                //String packageName = rs.getString("package_name");
                dc.setPackageName(packageName);
                String keyword = rs.getString("package_keyword");
                dc.setKeyword(keyword);
                String addonkeyword = rs.getString("addon_keyword");
                dc.setAddonkeyword(addonkeyword);
                String keywordMoMo = rs.getString("package_keyword_MoMo");
                dc.setKeywordMoMo(keywordMoMo);
                 String addonkeywordmomo = rs.getString("addon_keyword_momo");
                dc.setAddonkeywordmomo(addonkeywordmomo);
                double price = rs.getInt("package_price");
                dc.setPrice(price); 
                double addonprice = rs.getInt("addon_package_price");
                dc.setAddonprice(addonprice);
                
                String moMoTxt = rs.getString("package_MoMo_Text");
                dc.setMoMoTxt(moMoTxt);
                
                String creditTxt = rs.getString("package_Credit_Text");
                dc.setCreditTxt(creditTxt);
                
                 String addonheader = rs.getString("addon_header_Text");
                dc.setAddonheader(addonheader);
                
                String confirmationTxt = rs.getString("package_confirmation_Text");
                dc.setConfirmationTxt(confirmationTxt);
                
                String addonconfirmationTxt = rs.getString("addon_package_confirmation_Txt");
                dc.setAddonconfirmationTxt(addonconfirmationTxt);
                String Duration = rs.getString("DURATION");
                dc.setDuration(Duration);
                
               dataPackage =dc;
               DBTablePrinter.printResultSet(rs);
                
            }

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return dataPackage;
    }
    
      // Get the Momo Msisdn for free 500MB ! Project
    public List<MomoMsisdn> getMomoMsisdn() {
        List<MomoMsisdn> list = new ArrayList<MomoMsisdn>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append(" select * from MOMOMSISDN_CVM_500MB where STATUS = 0 ");
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

             while( rs.next() )
            {
                MomoMsisdn dc = new MomoMsisdn();
                String msisdn = rs.getString("MSISDN");
                dc.setMsisdn(msisdn);
                
                int Status = rs.getInt("STATUS");                
                dc.setStatus(Status);
                
                
                list.add(dc);
                DBTablePrinter.printResultSet(rs);
            }

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
     
    
    // Update
       
    public void updateMomoMsisdn(List<MomoMsisdn> momoStatusList) throws Exception{
            //AuditLog auditLog = null;    
      
        Connection conn = null;
        PreparedStatement pstmt =  null;
        String fileName = null;
        
        try{
            conn = getConnection();
            
            StringBuilder sb = new StringBuilder();
            sb.append("UPDATE APP_USER.MOMOMSISDN_CVM_500MB ");                        
            sb.append("SET STATUS = 2 ");
            sb.append("where STATUS = 0 ");

            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            conn.setAutoCommit(false);
            
            for(MomoMsisdn NumberStatus: momoStatusList){
                //
               
                pstmt.setString (1,  NumberStatus.getMsisdn());
                              
                Date createdDate = NumberStatus.getCreatedDate();
                java.sql.Timestamp createdTimestamp = new java.sql.Timestamp(createdDate.getTime());
                pstmt.setTimestamp(2, createdTimestamp);
                                            
                pstmt.addBatch();                
            }
            
            // execute insert SQL stetement
            int [] tt = pstmt.executeBatch();
            conn.commit();

        }catch(Exception e){
            
            try{conn.rollback();}catch(Exception f){}
            throw new Exception(fileName,e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
    }
    
      // Insertion of Transaction for free 500MB ! Project
   
    public int insertTransactionMomoCVM(String msisdn, String packageName, String keyword, int response) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append(" INSERT INTO  ");
            sb.append(" MOMOMSISDN_CVM_FREE_TRANSAC ( ");
            sb.append(" MOMO_MSISDN ,PACKAGE_NAME,PACKAGE_KEYWORD,RESPONSE) ");
            sb.append(" VALUES (  ?,   ?,   ?,   ? ) ");
            
            //String msisdn, String transactionId, String datakeyword, String response
                                   
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1, msisdn);
            pstmt.setString(2, packageName);
            pstmt.setString(3, keyword);
            pstmt.setInt(4, response);

            pstmt .executeUpdate();
           

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
    
       // Check if the suscriber got the free 500MB ! Project
    
     public List<MomoMsisdn> getProvisionnedMsisdn() {
        List<MomoMsisdn> list = new ArrayList<MomoMsisdn>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append(" select * from MOMOMSISDN_CVM_500MB where STATUS = 2 ");
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

             while( rs.next() )
            {
                MomoMsisdn dc = new MomoMsisdn();
                String msisdn = rs.getString("MSISDN");
                dc.setMsisdn(msisdn);
                
                int Status = rs.getInt("STATUS");                
                dc.setStatus(Status);
                
                
                list.add(dc);
                DBTablePrinter.printResultSet(rs);
            }

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
    
   
    
     // Get all the MOMOMSISDN
     public List<MomoMsisdn> getMomoClient() {
        List<MomoMsisdn> list = new ArrayList<MomoMsisdn>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
                        
            sb.append("select * from momomsisdn_cvm_500mb");
            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);
            
            while( rs.next() )
            {
                MomoMsisdn dc = new MomoMsisdn();
                String num = rs.getString("MSISDN");
                dc.setMsisdn(num);
                             
                
                list.add(dc);
                DBTablePrinter.printResultSet(rs);
                
            }

        }catch(Exception e){
           System.err.println("Got an exception! "); 
           System.out.println(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
       // Get all the Momo Client
     
 public List<MomoMsisdn> getMomoClients(List<MomoMsisdn> clients) {
        List<MomoMsisdn> list = new ArrayList<MomoMsisdn>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            HashMap deviceStore = new HashMap();
            StringBuilder sb = new StringBuilder();
            
            sb.append("SELECT * ");
//            sb.append("CREATED_DATE ");
            sb.append("FROM MOMOMSISDN_CVM_500MB  ");
            
            if((clients != null)&& (clients.size() > 0)){
                sb.append("WHERE MSISDN IN ( ");
                int cnt = clients.size();
                
                for(int i =0;i< cnt;i++){
                    MomoMsisdn dv = clients.get(i);
                    String momoNumber = dv.getMsisdn();
//                    String dmcImei = dv.getImei();
                    deviceStore.put(momoNumber, dv);
                    if (cnt == (i+1)){
                        sb.append("'"+momoNumber+"' ");
                    }else{
                        sb.append("'"+momoNumber+"', ");
                    }
                }
                sb.append(") ");
            }
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                MomoMsisdn dmc = new MomoMsisdn();
                dmc.setMsisdn(rs.getString("MSISDN"));
             
                dmc.setStatus( rs.getInt("STATUS"));
               
                               
                list.add(dmc);
                
            }

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
 
 public List<Package> getTransactionsRenewalList(String packageType, String result, String renewal_response) {
        List<Package> resp = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("select * from databundle_revamp_transact where package_type ='"+packageType+"'and result='"+result+"' and renewal_response ='"+renewal_response+"'");
          
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                Package dc = new Package();
                dc.setPackageType(packageType);
                dc.setResult(result);
                dc.setRenewlal_status(renewal_response);
                String transaction_id = rs.getString("transaction_id ");
                dc.setTransaction_id(transaction_id);
                String customer_msisdn = rs.getString("customer_msisdn");
                dc.setCustomer_msisdn(customer_msisdn);
                String keyword = rs.getString("refillid");
                dc.setKeyword(keyword);
                String package_name = rs.getString("package_name");
                dc.setPackageName(package_name);
                String duration = rs.getString("duration");
                dc.setDuration(duration);
                java.sql.Timestamp expiry_date= rs.getTimestamp("expiry_date");
                dc.setExpiry_date(expiry_date);
                java.sql.Timestamp renewal_date= rs.getTimestamp("renewal_date");
                dc.setRenewal_date(renewal_date);
                double price = rs.getDouble("price");
                dc.setPrice(price); 
               
                resp.add(dc);
                DBTablePrinter.printResultSet(rs);
            }

        }catch(Exception e){
            System.err.println("Got an exception! "); 
            System.out.println(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }    
    
    public int updateDailyTransaction (List<Package> transactions) throws SQLException,ClassNotFoundException,InstantiationException,IllegalAccessException {
        //AuditLog auditLog = null;    
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        String fileName = null;
        
        try{
            conn = getConnection();
            
            StringBuilder sb = new StringBuilder();
            sb.append("UPDATE databundle_revamp_transact ");                        
            sb.append("SET expiry_date  = ?  renewal_date   = ? renewlal_status  = ?");
            sb.append("where transaction_id = ? ");
            sb.append("and refillid = ? ");
           
            
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            conn.setAutoCommit(false);
            
//            for(Package transaction : transactions){
//                Date d = transaction.getClearanceDate();
//                java.sql.Timestamp t = new java.sql.Timestamp(d.getTime());
//                pstmt.setTimestamp(1, t);
//                pstmt.setInt (2, transaction.getMSISDN() );
//                pstmt.setInt (3, transaction.getDA() );
//                pstmt.addBatch();                
//            }
            
            // execute insert SQL stetement
            int [] tt = pstmt.executeBatch();
            conn.commit();
            
                  

        }catch(SQLException e){
            
            try{conn.rollback();}catch(Exception f){}
            throw new SQLException(fileName,e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
    

    
    
}
